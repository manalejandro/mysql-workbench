# Mysql-Workbench

```
Mysql-Workbench contained in Docker with X11 shared GUI
```

## Install

```
git clone --recurse-submodules https://git.manalejandro.com/ale/mysql-workbench
cd mysql-workbench
docker-compose up -d
```

## Capture

![mysql-workbench](mysql-workbench.jpg)

## License

```
MIT
```
